
import ExchangeCenter from './currency/exchangecenter';
const ExCenter = new ExchangeCenter();

import database_handler from './postgres/database_handler';
import Company from './modules/company';
import WebhookConnection from './webhook/webhookConnection';
const database = new database_handler(ExCenter);

async function start() {
   /* let comp = await database.getCompanyDatabase().getCompanyWithAPIClient('ToI/80LtQ+z6pw==');
    let wConnection = new WebhookConnection(comp, ExCenter);
    wConnection.relayUpdate('InvoicePayment', '6d4d0561fea849ac', 0, 0, 'NAH');*/

    //Webhook Retry loop
    setInterval(async () => {
        let failedUpdates = await database.getWebhookUpdatesDatabase().getFailedUpdates();
        for(let i = 0; i < failedUpdates.length; i++) {
            let updateCompany = await database.getCompanyDatabase().getCompany(failedUpdates[i].company);
            let wConnection = new WebhookConnection(updateCompany, ExCenter);
            await wConnection.retryUpdate(failedUpdates[i].id, failedUpdates[i].payload);
        }
    }, 1000 * 10);
}
start();