import Company from "../modules/company";
import ExchangeCenter from "../currency/exchangecenter";
import database_handler from "../postgres/database_handler";
import crypto from "crypto";
import e from "express";
const fetch = require('node-fetch');

export default class WebhookConnection {

    private company: Company;
    private database: database_handler;
    constructor(company: Company, ExchangeCenter: ExchangeCenter) {
        this.company = company;
        this.database = this.database = new database_handler(ExchangeCenter);
    }

    public async relayUpdate(eventType: "InvoiceCreated" | "InvoicePayment" | "InvoiceSettlement", invoiceid: string, amountAUD: number, amountCurrency: number, currency: string) {
        return new Promise<boolean>(async resolve => {
            let hmacSeed = this.company.getHMAC();
            let host = this.company.getWebhookHost();
            const hmac = crypto.createHmac('sha1', hmacSeed);
            let invoice = await this.database.getInvoicesDatabase().getCompanyInvoice(this.company, invoiceid);
            let payload = {
                event: eventType,
                amountAUD: amountAUD,
                amountCurrency: amountCurrency,
                currency: currency,
                invoice: invoice
            };
            let webhookUpdateID = await this.database.getWebhookUpdatesDatabase().createWebhookUpdate(this.company, payload);
            let updateResponse = await this.sendUpdate(host, hmac, webhookUpdateID, payload);
            if(updateResponse) {
                return resolve(true);
            } else {
                await setTimeout(async () => {
                    let updateResponseB = await this.sendUpdate(host, hmac, webhookUpdateID, payload);
                    return resolve(updateResponseB);
                }, 100);
            }
        });
    }

    public async retryUpdate(webhookUpdateID: string, payload: any) {
        return new Promise<boolean>(async resolve => {
            let hmacSeed = this.company.getHMAC();
            let host = this.company.getWebhookHost();
            const hmac = crypto.createHmac('sha1', hmacSeed);
            let updateResponse = await this.sendUpdate(host, hmac, webhookUpdateID, payload);
            return resolve(updateResponse);
        });
    }

    private async sendUpdate(host: string, hmac: crypto.Hmac, webhookUpdateID: string, payload: any):Promise<boolean> {
        return new Promise<boolean>(async resolve => {
            const digest = 'sha1=' + hmac.update(JSON.stringify(payload)).digest('hex');
            let startTime = (new Date()).getTime();
            fetch(host, {
                method: 'post',
                body: JSON.stringify(payload),
                headers: {
                    'Content-Type': 'application/json',
                    'XVERIF': digest }
            }).then(async (res:any) => {
                if(res.status == 200 && res.statusText == 'OK') {
                    let endTime = (new Date()).getTime();
                    await this.database.getWebhookUpdatesDatabase().markDelivered(webhookUpdateID, endTime - startTime);
                    return resolve(true);
                } else {
                    await this.database.getWebhookUpdatesDatabase().markFailure(webhookUpdateID);
                    return resolve(false);
                }
            }).catch(async (err:any) => {
                await this.database.getWebhookUpdatesDatabase().markFailure(webhookUpdateID);
                return resolve(false);
            });
        });
    } 

}