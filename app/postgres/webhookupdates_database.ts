import {Pool, PoolClient} from 'pg';
import Invoice from '../invoice/invoice';
import companyDatabase from './company_database';
import Coin from '../currency/coin';
import ExchangeCenter from '../currency/exchangecenter';
import Company from '../modules/company';

export default class webhookUpdatesDatabase {

    private pool : Pool;
    private company_database:companyDatabase;

    constructor(pool : Pool) {
        this.pool = pool;
        this.company_database = new companyDatabase(pool);
    }


    /**
     * Creates the webhook updates table.
     * @returns {promise} Returns a promise, which resolves as true or false depending on the result. 
     */
    public createWebhookUpdatesTable = () => {
        var _this = this;
        return new Promise(function(resolve, reject) {
            _this.pool.query(`CREATE TABLE public.webhookupdates
            (
                id SERIAL,
                company character varying(120) COLLATE pg_catalog."default",
                payload jsonb,
                delivered boolean,
                createdtime timestamp with time zone,
                deliveredtime timestamp with time zone,
                responsetime integer,
                attempts integer,
                lastattempt timestamp with time zone,
                CONSTRAINT webhookupdates_pkey PRIMARY KEY (id),
                CONSTRAINT webhookupdates_company_fkey FOREIGN KEY (company)
                    REFERENCES public.company (uuid) MATCH SIMPLE
                    ON UPDATE NO ACTION
                    ON DELETE NO ACTION
            )`, (err:any, res:any) => {
                if (err){ console.log(err); return resolve(false); }
                return true;
            });
        });
    }

    /**
     * Used to create a new webhook update
     */
    public createWebhookUpdate(comp: Company, payload: any):Promise<string> {
        var _this = this;
        return new Promise(async function(resolve, reject) {
            try  {
                await _this.pool.query(`INSERT INTO public.webhookupdates(
                    company, payload, delivered, createdtime, attempts, lastattempt)
                    VALUES ($1, $2, $3, now(), $4, now()) RETURNING id;`, [comp.getUUID(), payload, false, 0], async (err:any, res:any) => {
                        let id = res.rows[0].id;
                        return resolve(id);
                    });
            } catch (e) {
                console.log(e);
                return resolve('false');
            }
        });
    }

    /**
     * Used to mark a webhook as being delivered.
     * @param webhookid The webhook ID to mark as completed
     */
    public markDelivered(webhookid: string, responseTime: number) {
        var _this = this;
        return new Promise(async function(resolve, reject) {
            try  {
                await _this.pool.query(`UPDATE public.webhookupdates set attempts = attempts + 1, delivered = true, deliveredtime = now(), responsetime = $2 where id = $1`, [webhookid, parseInt(responseTime.toString())], async (err:any, res:any) => {
                        return resolve(true);
                    });
            } catch (e) {
                console.log(e);
                return resolve(false);
            }
        });
    }

    /**
     * Used to mark a failed attempt at sending a webhook an update.
     * @param webhookid The webhook ID to mark as failed.
     */
    public markFailure(webhookid: string) {
        var _this = this;
        return new Promise(async function(resolve, reject) {
            try  {
                await _this.pool.query(`UPDATE public.webhookupdates set attempts = attempts + 1, lastattempt = now() where id = $1`, [webhookid], async (err:any, res:any) => {
                        return resolve(true);
                    });
            } catch (e) {
                console.log(e);
                return resolve(false);
            }
        });
    }

    /**
     * Will return all of the failed updates, used to redeliver.
     */
    public getFailedUpdates():any {
        var _this = this;
        return new Promise(async function(resolve, reject) {
            try  {
                await _this.pool.query(`SELECT * from webhookupdates where lastattempt < now() - interval '1 minute' and delivered = false`, [], async (err:any, res:any) => {
                        return resolve(res.rows);
                    });
            } catch (e) {
                console.log(e);
                return resolve(false);
            }
        });
    }


}