const Sentry = require('@sentry/node');
Sentry.init({ dsn: 'https://58c760d17c60405db13f92bbe1744a89@sentry.io/1497974' });
import ExchangeCenter from './currency/exchangecenter';
const ExCenter = new ExchangeCenter();
ExCenter.calculateRates();
ExCenter.startPriceHeartBeat();

import database_handler from './postgres/database_handler';
const database = new database_handler(ExCenter);

import WebhookConnection from './webhook/webhookConnection';

import Invoice from './invoice/invoice';

import express from "express";
import bodyParser from 'body-parser';
import Company from './modules/company';
const hpp = require('hpp');
const helmet = require('helmet');
const contentLength = require('express-content-length-validator');
const cors = require("cors");  
const app = express();

//**  Security Middleware */
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(hpp());
app.use(cors());
app.use(helmet());
app.use(helmet.hidePoweredBy({setTo: 'Vodka'}));
app.use(contentLength.validateMax({max: 9999, status: 400, message: "I see how it is. watch?v=ewRjZoRtu0Y"}));
app.use('/server', require('./routes/server'));
app.use('/webhook', require('./routes/webhook'));
app.use('/api/v1', require('./routes/api'));

app.get('/', (req, res) => {
  res.send('Hello World - Changed, again!!');
});

app.listen(8001, '127.0.0.1', async () => {
  await database.setupUserDB();
  await database.setupServersDB();
  console.log('AUCRYPTO - Transaction worker started → PORT 8001');

  //Webhook Retry loop
  setInterval(async () => {
      let failedUpdates = await database.getWebhookUpdatesDatabase().getFailedUpdates();
      for(let i = 0; i < failedUpdates.length; i++) {
          let updateCompany = await database.getCompanyDatabase().getCompany(failedUpdates[i].company);
          let wConnection = new WebhookConnection(updateCompany, ExCenter);
          await wConnection.retryUpdate(failedUpdates[i].id, failedUpdates[i].payload);
      }
  }, 1000 * 10);
});